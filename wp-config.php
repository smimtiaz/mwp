<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test-maruf');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IH0IG:MC`HIBL|XLuh{E.R|02Es44JmUh{tj6kn;lJsw9OzeHs,. 9HfMX@+dH^0');
define('SECURE_AUTH_KEY',  '`F+a6~M{f8N_ai2<;Iq.2faVsEWom=_:XeZF(|:M=x UO>]G3`,j4|fZtts6vKG]');
define('LOGGED_IN_KEY',    'cF6yR%IJ{=d9;>!Ge2E]0Wk!-y72x@@!+<oRPK)w`M.Ul:bKG+ FM(vjctXc1t<y');
define('NONCE_KEY',        '~^;P)Zt69yxe$5jUJ4&$f^L`%^=Co6Lg^GIGng9[)r}qv(E./6]<V1IrdPuxT>4o');
define('AUTH_SALT',        '! e:iXVCO: }SDK@ewbbWQP~VuY[IB_cOFlIg]U&l0&2)E+&~GdwUVtp3-/n<m]Z');
define('SECURE_AUTH_SALT', 'yb!JU^JTHA:;I4`bp~zj[&/ /X}#4kdI1i.rKZ8.]VAi)$@qj^Pn*7_Ao]^pRdn{');
define('LOGGED_IN_SALT',   'D])9-|.&l-in/W*~Xd%5HN?#Q$z{^h-cxc,qX =7%z2$2.cyiCExMo]J.iyRB6vV');
define('NONCE_SALT',       'C|}8CgH{_jC B|<oMD,^bwUejtWAk;C$oKt(my|++ ?UvwVf4dbH2#d#&7!C ud{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
